var 
    AWS = require("aws-sdk"),                              
    DDB = new AWS.DynamoDB({
        apiVersion: "2012-08-10",
        region: "us-east-1"
    });                                           

(function createADataBaseTable(){
    var 
        params = {
           AttributeDefinitions: [{
                AttributeName: "userID", 
                AttributeType: "S"
            }], 
            KeySchema: [{
                AttributeName: "userID", 
                KeyType: "HASH"
            }], 
            ProvisionedThroughput: {
                ReadCapacityUnits: 1, 
                WriteCapacityUnits: 1
            }, 
            TableName: "firstTable"
    };
    DDB.createTable(params, function(err, data){
        console.log(err, data);             
    });
})();
